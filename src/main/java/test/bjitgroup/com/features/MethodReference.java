package test.bjitgroup.com.features;

import java.io.IOException;

@FunctionalInterface
interface FInterface{
    int calculate(int a, int b);
}

class Instance {

    public static int sum(int a, int b){
        return  a+b;
    }

}


public class MethodReference {

    public void print(){
        System.out.println("heloo...");
    }

    public static void main(String[] args) throws IOException {
//---1. Reference To an instance method--------
//        Instance instance=new Instance();
//        FInterface fInterface=instance::sum;

//---2. Reference to a static method-----------
//        FInterface fInterface=Instance::sum;
//        System.out.println(fInterface.calculate(4,3));


//--- New String Utility methods

//        String name="";
//        System.out.println(name.isBlank());
//        name="hello";
//        System.out.println(name.isBlank());

//        String name="Hello\nWorld";
//        List list=name.lines().collect(Collectors.toList());
//        System.out.println(list);


//        String name="   Hello  World  ";
//        System.out.println('*'+name+'*');
//        System.out.println('*'+name.strip()+'*');
//        System.out.println('*'+name.stripLeading()+'*');
//        System.out.println('*'+name.stripTrailing()+'*');

//        String name="Hello";
//        System.out.println(name.repeat(3));

//        Path filepath= Paths.get("/myFile.txt");
//        Files.writeString(filepath,"Hello World...Test Java features");
//        String content= Files.readString(filepath);
//        System.out.println(content);

    }
}
