package test.bjitgroup.com.features;

interface LivingThings{
    default void print(){
        System.out.println("Living Things");
    }
}

interface animal extends LivingThings{
    default void print(){
        System.out.println("Animal");
    }

}

interface Pet extends LivingThings{
    default void print(){
        System.out.println("Pet");
    }
}


public class DefaultMethodInInterface implements LivingThings{
    public static void main(String[] args) {
            DefaultMethodInInterface defaultMethodInInterface=new DefaultMethodInInterface();
            defaultMethodInInterface.print();
    }

}
