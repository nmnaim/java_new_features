package test.bjitgroup.com.features;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

//
//public class Demo {
//    public static void main(String[] args) {
//        Random rand = new Random();
////        int[]  a=new int[100];
//        List<Integer> list = new ArrayList<>();
//        for(int i=0;i<100;i++){
//            list.add(rand.nextInt(100)+1);
//        }
//        Collections.sort(list);
//        list.forEach(System.out::println);
//    }
//}
interface MyInterface{
//    default void display() {
//        System.out.println("Hello");
//    }
    int  BB(int x);
}

public class Demo {
//    public void myMethod(){
//        System.out.println("Instance Method");
//    }
//    public static void main(String[] args) {
//        Demo obj = new Demo();
//        // Method reference using the object of the class
//        MyInterface ref = obj::myMethod;
//        // Calling the method of functional interf
//        ref.display();

//    }
public static void main(String[] args) {
//    List<Integer> list=Arrays.asList(1,2,3,4,5);
//    int sum= list.stream().reduce(0,(total,element)-> element+total);
//    System.out.println(sum);
//    MyInterface myInterface=(var x)-> x+1;
//    System.out.println(myInterface.BB(2));
    List<Integer> list=Arrays.asList(1,5,6,7,8,9,10);
    System.out.println(list.stream().filter(i-> (i%2)==0));
}

public void StringFeatures(){
    String str1 = "";
    System.out.println(str1.isBlank());

    String str2 = "test";
    System.out.println(str2.isBlank());

    String str3 = "nayeem";
    System.out.println(str3.repeat(5));


    Optional str4 = Optional.empty();
    str4.isEmpty();

    Optional str5 = Optional.of("TonyStark");
    str5.isEmpty();
}




}