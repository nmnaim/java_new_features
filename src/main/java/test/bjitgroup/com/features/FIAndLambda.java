package test.bjitgroup.com.features;

import java.util.stream.IntStream;

@FunctionalInterface
interface FunctionalInt{
    int calculate(int a, int b);
}

public class FIAndLambda{
    public static void main(String[] args) {
        FunctionalInt functionalInt=(a,b)-> a+b;
        System.out.println(functionalInt.calculate(5,6));

        IntStream.of(1, 2, 3, 5, 6, 7)
                .filter((var i) -> i % 2 == 0)
                .forEach(System.out::println);
    }
}
